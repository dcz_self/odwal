Odwal
====

*Odwal* is a simple way to put up and tear down resources. Like [make](https://www.gnu.org/software/make/), except both ways.

The supplied commands are useful for mounting offline backups, but it's by no means limited to that.

Using *odwal*
----------------

Imagine you have a USB hard drive with a single encrypted partition. Create the following `odwal.to` file:

```
(defstep usbmain
  (BlockStorage
    args (id "ata-my_drive") ; here enter filename from /dev/disk/by-id
  )
)

(Mount
    args (
        fslabel "monthly_backup" ; from /dev/disk/by-partlabel
        destination "/backups/monthly"
    )
    parents ((EncryptedBlock
        args (
            path "/dev/disk/by-partlabel/monthly_encrypted" ; from /dev/disk/by-partlabel
            name "monthly" ; name for device mapper
        )
        parents (usbmain)
    ))
)
```

This creates a nice list of targets:

```
$ python3 -m odwal ./odwal.to list
BlockStorage:ata-my_drive
EncryptedBlock:monthly
Mount:fslabel:monthly_backup
```

What you care of course is the final one, which you can set up using:

```
$ sudo python3 -m odwal ./odwal.to hold Mount:fslabel:monthly_backup
Checking: Mount:fslabel:monthly_backup
Checking: EncryptedBlock:monthly
[...]
Enter passphrase for /dev/sdc:
[...]
Press enter to tear down
```

After pressing enter, all will be undone in the reverse order.

Installation
-------------

You need [Python 3](https://www.python.org/) and [sexpdata](https://pypi.org/project/sexpdata/).

Adding your own resources
-------------------------------

Resources that odwal can acquire and release are all inside the `steps.py` file, as classes.

Copying
---------

*Odwal* is available under the terms of the AGPL license, version 3 or later.
