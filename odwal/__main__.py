"""Copyright (C) 2021 dcz <gilaac.dcz@porcupinefactory.org>
SPDX-License-Identifier: AGPL-3.0+
"""

import sexpdata

from . import parse


def teardown_info(tasks, dry=False):
    for task in tasks:
        print('Tear down', task.get_id())
        if not dry:
            task.tear_down()


def tear_down_list(tasktree, tasks, dry=False):
    for idx in tasks:
        teardown_info(tasktree.tear_down_one(idx), dry)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    subparsers = parser.add_subparsers(dest='command')#, required=True)
    parser_list = subparsers.add_parser('list')
    parser_hold = subparsers.add_parser('hold')
    parser_hold.add_argument('tasks', nargs='+')
    parser_hold.add_argument('--dry', action='store_true')
    parser_hold = subparsers.add_parser('clear')
    parser_hold.add_argument('tasks', nargs='*')
    parser_hold.add_argument('--dry', action='store_true')
    args = parser.parse_args()
    path = args.config
    data = sexpdata.parse(open(path).read())
    datatree = parse.parse_trees(data)
    tasktree = parse.instantiate(datatree)

    if args.command == 'list':
        for name in tasktree.get_names():
            print(name)
    elif args.command == 'hold':
        built_tasks = []
        task_indices = [tasktree.find_by_id(name) for name in args.tasks]
        for idx in task_indices:
            for task, tidx, do_setup in tasktree.set_up(idx):
                if do_setup:
                    print("Set up", task.get_id())
                    built_tasks.append(tidx)
                    if not args.dry:
                        task.set_up()
                else:
                    print("Checking:", task.get_id())
        input("Press enter to tear down")
        tear_down_list(tasktree, built_tasks, args.dry)
    elif args.command == 'clear':
        selected_tasks = [tasktree.find_by_id(name) for name in args.tasks]
        if selected_tasks:
            tear_down_list(tasktree, selected_tasks, args.dry)
        else:
            teardown_info(tasktree.tear_down_all(), args.dry)
