"""Copyright (C) 2021 dcz <gilaac.dcz@porcupinefactory.org>
SPDX-License-Identifier: AGPL-3.0+
"""

"""Module for reading config files with steps."""

from sexpdata import Symbol
import sexpdata

from . import steps
from .steps import Reference


def pairs(list_):
    if len(list_) % 2 != 0:
        raise ValueError("List can't be split into pairs, it's odd.")
    return zip(list_[::2], list_[1::2])


def symdict(pairs):
    def extract(s):
        if not isinstance(s, Symbol):
            raise ValueError("{} is not a symbol".format(s))
        return s.value()
    return dict((extract(s), v) for s, v in pairs)


def parse_root(sexpdata):
    """Parse parameters of a single task"""
    if not isinstance(sexpdata, list):
        raise ValueError("{} is not a task".format(sexpdata))
    try:
        kind_name = sexpdata[0]
    except IndexError:
        raise ValueError("task must at least have a name")
    params = sexpdata[1:]
    if not isinstance(kind_name, Symbol):
        raise ValueError("{} is not a valid task kind".format(kind_name))
    kind_name = kind_name.value()
    params = symdict(pairs(params))
    args = params.pop('args', [])
    if not isinstance(args, list):
        raise ValueError("args must be a list")
    args = symdict(pairs(args))
    parents = params.pop('parents', [])
    if not isinstance(parents, list):
        raise ValueError("parents must be a list")
    for key, value in args.items():
        if isinstance(value, Symbol):
            parent_idx, keyname = value.value().split('.', maxsplit=2)[1:]
            args[key] = Reference(int(parent_idx), keyname)
    if params:
        raise ValueError("Unknown arguments to {}: {}".format(kind_name, ', '.join(params.keys())))
    return kind_name, args, parents


def parse_trees(sexpdata):
    """Parses entire tree of tasks"""
    def parse_stmt(stmt):
        if stmt[0].value() == 'defstep':
            return stmt[1].value(), stmt[2]
        return None, stmt
    stmts = (parse_stmt(stmt) for stmt in sexpdata)

    def parse_task(sexpdata):
        if isinstance(sexpdata, Symbol):
            return sexpdata
        kind_name, args, parents = parse_root(sexpdata)
        return kind_name, args, [parse_task(parent) for parent in parents]

    parsed = ((label, parse_task(task)) for label, task in stmts)

    tasks = []
    labeled_tasks = {}

    def flatten_task(kind_name, args, parents):
        parent_idxs = []
        for parent in parents:
            if isinstance(parent, Symbol):
                parent_idxs.append(labeled_tasks[parent.value()])
            else:
                parent_idxs.append(flatten_task(*parent))
        tasks.append((kind_name, args, parent_idxs))
        return len(tasks) - 1

    for label, task in parsed:
        idx = flatten_task(*task)
        if label is not None:
            labeled_tasks[label] = idx

    return tasks


def instantiate(tree):
    """Builds the actual tasks out of parsed data"""
    parents = []
    for kind_name, args, parent_idxs in tree:
        parents.append(parent_idxs)
    
    steps_ = [None] * len(tree)
    
    def add_index(idx):
        if steps_[idx] is not None:
            return
        kind_name, args, parent_idxs = tree[idx]
        cls = getattr(steps, kind_name)
        if not issubclass(cls, steps.Step):
            raise ValueError("{} is not a valid Step".format(kind_name))
        for parent_idx in parent_idxs:
            add_index(parent_idx)
        parents_ = [steps_[pi] for pi in parent_idxs]
        steps_[idx] = cls.build(parents_, args)
    
    for i, _ in enumerate(tree):
        add_index(i)

    tt = steps.TaskTree()
    tt.steps = steps_
    tt.parents = parents
    return tt
