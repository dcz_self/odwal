"""
Odwal is a different "make".

The goal is to facilitate backups: set up, perform some operations (maybe schedule considering their resource usage), and then tear down what was set up.

The main difference to make is that it not only needs to reach a particular state, but also to tear it down.
"""
