"""Copyright (C) 2021 dcz <gilaac.dcz@porcupinefactory.org>
SPDX-License-Identifier: AGPL-3.0+
"""

"""
This module defines Steps: resources that odwal will acquire or release.

Steps contain the instructions to create and tear down the resources,
so they are more like edges than nodes in the dependency graph.

Either way, resources are specified in the config files,
and how to get them are Steps.
"""

from dataclasses import dataclass
import glob
import os
from pathlib import Path
import subprocess
from time import sleep


def readlink(path):
    return Path(os.readlink(path.as_posix()))


def is_mount(path):
    return os.path.ismount(path.as_posix())


@dataclass
class Reference:
    """Parameter referring to parent's provides"""
    parent_idx: int
    key: str


class Step:
    """Subclass this to create a new resource step.

    Each Step subclass that is inside the toplevel scope of this file
    will be available in config scripts under that name.
    
    Simply said, put your `class MyResource(Step)` inside this file.
    """
    def __init__(self, parameters):
        self.parameters = parameters
    def needs_set_up(self):
        return not self.is_complete()
    def needs_tear_down(self):
        return self.is_complete()
    def is_complete(self):
        pass
    def set_up(self):
        pass
    def tear_down(self):
        pass

    provides = {} # variables made available to steps built on this one
    parameters = {} # variables in use
    @classmethod
    def build(cls, parents, parms):
        resolved_parms = {}
        for name, parm in parms.items():
            if isinstance(parm, Reference):
                parm = parents[parm.parent_idx].provides[parm.key]
            resolved_parms[name] = parm
        ret = cls(resolved_parms)
        return ret
    @classmethod
    def clsname(cls):
        return cls.__name__


class Process:
    """Makes sure a process is running"""
    def is_complete(self):
        return False # not sure how to detect a generic process
    def set_up(self):
        self.process = subprocess.run(self.parameters['command'])
        self.provides['pid'] = self.process.pid()
    def tear_down(self):
        self.process.kill()


class BlockStorage(Step):
    """Removable storage."""
    def __init__(self, parameters):
        Step.__init__(self, parameters)
        self.path = Path('/dev/disk/by-id/') \
            .joinpath(self.parameters['id'])
        self.provides['path'] = self.path.as_posix()

    def is_complete(self):
        return self.path.exists()

    def set_up(self):
        # Normally there's nothing to do to set up the device
        if self.needs_set_up():
            raise RuntimeError("Device not present at {}".format(self.path))

    def get_id(self):
        return '{}:{}'.format(self.clsname(), self.parameters['id'])

    def needs_set_up(self):
        return not self.path.exists()

    def needs_tear_down(self):
        return not subprocess.check_output(['hdparm', '-C', self.path.as_posix()]).strip().endswith(b'standby')

    def tear_down(self):
        subprocess.run(['sync'], check=True)
        # alternatively, hdparm -f && hdparm -F && hdparm -S
        subprocess.run(['sg_sync', self.path.as_posix()], check=True)
        subprocess.run(['sg_start', '--eject', self.path.as_posix()], check=True)


class WarmPlugSATA(BlockStorage):
    """Warm- or hot-plugged hard drive on the SATA bus.
    Detaches the drive on teardown."""
    def set_up(self):
        hosts = glob.glob('/sys/class/scsi_host/host*/')
        free_hosts = [host for host in hosts if not glob.glob(host + '/device/target*')]
        for bus in free_hosts + hosts:
            open(bus + '/scan', 'w').write('- - -\n')
            if self.is_complete():
                return
        sleep(5) # allow for spinup
        if self.is_complete():
            return
        raise RuntimeError("Drive did not appear after rescan")

    def needs_tear_down(self):
        return self.path.exists()

    def tear_down(self):
        with open('/sys/block/{}/device/delete'.format(readlink(self.path).name), 'w') as o:
            o.write('1\n')


class EncryptedBlock(Step):
    def __init__(self, parameters):
        Step.__init__(self, parameters)
        self.path = Path('/dev/mapper/') \
            .joinpath(self.parameters['name'])
        self.provides['path'] = self.path.as_posix()

    def get_id(self):
        return '{}:{}'.format(self.clsname(), self.parameters['name'])

    def is_complete(self):
        return self.path.exists()

    def set_up(self):
        subprocess.run(['cryptsetup', 'open',
            self.parameters['path'], self.parameters['name']],
            check=True)

    def tear_down(self):
        subprocess.run(['cryptsetup', 'close', self.parameters['name']],
            check=True)


class Partitions(Step):
    """Works on /dev/mapper/"""
    def get_id(self):
        return 'Partitions:{}'.format(self.parameters['path'])
    def is_complete(self):
        # TODO: check for parents I guess?
        path = self.parameters['path']
        if path[-1] in map(str, range(10)):
            path = path + 'p'
        return Path(path + '1').exists()

    def set_up(self):
        subprocess.run(['kpartx', '-avs', self.parameters['path']], check=True)
    def tear_down(self):
        subprocess.run(['kpartx', '-dvs', self.parameters['path']], check=True)


class Mount(Step):
    """
    (Mount
        args (
            fslabel "optional"
            path "optional"
            destination "/path")
        parents (parts)
    )
    """
    def __init__(self, parameters):
        Step.__init__(self, parameters)
        fslabel = self.parameters.get('fslabel', None)
        path = self.parameters.get('path', None)
        if fslabel is not None:
            self.block = Path('/dev/disk/by-label/') \
                .joinpath(fslabel)
            self.id = 'fslabel:' + fslabel
        elif path is not None:
            self.block = Path(path)
            self.id = path
        self.provides['path'] = self.parameters['destination']

    def get_id(self):
        return 'Mount:{}'.format(self.id)
    def is_complete(self):
        return is_mount(Path(self.parameters['destination']))
    def set_up(self):
        Path(self.parameters['destination']).mkdir(exist_ok=True)
        subprocess.run(['mount',
                        self.block, self.parameters['destination']],
                       check=True)
    def tear_down(self):
        subprocess.run(['umount', self.block], check=True)


class TaskTree:
    # Maybe should remember the state, e.g. which leaves are requested.
    # Then it could release them instead of tearing down.
    steps = [Step]
#    active_steps = [Step]
    parents = [[]]

    def get_names(self):
        for step in self.steps:
            yield step.get_id()

    def find_by_id(self, name):
        for i, step in enumerate(self.steps):
            if step.get_id() == name:
                return i
        raise ValueError("No step named {}".format(name))

    def set_up(self, step_index):
        # Traverses the tree depth-wise down, and then back up.
        # Assumes steps are cheap.
        step = self.steps[step_index]
        yield step, step_index, False
        if not step.needs_set_up():
            return
        for parent in self.parents[step_index]:
            yield from self.set_up(parent)
        yield step, step_index, True

    def tear_down_one(self, step_index):
        """Tears down this step and all building on this one if needed."""
        step = self.steps[step_index]
        if not step.needs_tear_down():
            return

        childs = [i for i, parents in enumerate(self.parents)
                  if step_index in parents]
        for child in childs:
            yield from self.tear_down_one(child)
        yield step
    
    def tear_down_all(self):
        for i, step in enumerate(self.steps):
            yield from self.tear_down_one(i)
